package memory

import (
	"encoding/json"
	"os"

	"github.com/google/uuid"
	"gitlab.com/VFay/starcraft-wiki/data"
)

type Storage struct {
	heroes []data.Hero
	quotes []data.Quote
}

func (s *Storage) Init() {
	file, _ := os.ReadFile("heroes.json")

	heroes := []data.Hero{}
	_ = json.Unmarshal([]byte(file), &heroes)

	s.heroes = heroes
}

func (s *Storage) GetAllHeroes() []data.Hero {
	var heroes []data.Hero

	for i := range s.heroes {
		hero := data.Hero{
			ID:   s.heroes[i].ID,
			Name: s.heroes[i].Name,
			Game: s.heroes[i].Game,
		}

		heroes = append(heroes, hero)
	}

	return heroes
}

func (s *Storage) GetHero(id string) data.Hero {
	var hero data.Hero

	for i := range s.heroes {
		if s.heroes[i].ID == id {
			hero.ID = s.heroes[i].ID
			hero.Name = s.heroes[i].Name
			hero.Game = s.heroes[i].Game

			return hero
		}
	}

	return hero
}

func (s *Storage) AddHero(hero data.Hero) {
	newId := uuid.New().String()

	newHero := data.Hero{
		ID:   newId,
		Name: hero.Name,
		Game: hero.Game,
	}

	s.heroes = append(s.heroes, newHero)
}

/*
func (s *Storage) UpdateHero(hero handlers.Hero) {
	for i := range s.heroes {
		if s.heroes[i].ID == hero.ID {
			s.heroes[i].Name = hero.Name
			s.heroes[i].Game = hero.Game

			return
		}
	}
}

func (s *Storage) RemoveHero(heroId string) {
	for i := range s.heroes {
		if s.heroes[i].ID == heroId {
			s.heroes = append(s.heroes[:i], s.heroes[i+1:]...)

			return
		}
	}
}

func (s *Storage) GetHeroQuates(heroId string) []Quote {
	var quotes []Quote

	for i := range s.quotes {
		if s.quotes[i].HeroID == heroId {
			quote := Quote{
				ID:     s.quotes[i].ID,
				HeroID: s.quotes[i].HeroID,
				Text:   s.quotes[i].Text,
			}

			quotes = append(quotes, quote)
		}
	}

	return quotes
}

func (s *Storage) AddHeroQuote(heroId string, text string) {
	found := false
	for i := range s.heroes {
		if s.heroes[i].ID == heroId {
			found = true
		}
	}

	if found {
		newId := uuid.New().String()

		newQuote := Quote{
			ID:     newId,
			HeroID: heroId,
			Text:   text,
		}

		s.quotes = append(s.quotes, newQuote)
	}
}

func (s *Storage) UpdateQuote(Id string, text string) {
	for i := range s.quotes {
		if s.quotes[i].ID == Id {
			s.quotes[i].Text = text

			return
		}
	}
}*/
