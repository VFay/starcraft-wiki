package data

type Hero struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Game string `json:"game"`
}

type Quote struct {
	ID     string
	HeroID string
	Text   string
}
