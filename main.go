package main

import (
	"flag"
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/VFay/starcraft-wiki/data"
	"gitlab.com/VFay/starcraft-wiki/services"
	"gitlab.com/VFay/starcraft-wiki/storage/memory"
)

func main() {
	listerAddr := flag.String("a", "9000", "")
	flag.Parse()

	var service services.Service
	storage := new(memory.Storage)

	storage.Init()

	service = services.NewService(storage)

	app := fiber.New()

	app.Get("getheroes", func(c *fiber.Ctx) error {
		heroes := service.GetHeroes()

		return c.JSON(heroes)
	})

	app.Get("hero/:id", func(c *fiber.Ctx) error {
		hero := service.GetHero(c.Params("id"))

		return c.JSON(hero)
	})

	app.Post("hero/create", func(c *fiber.Ctx) error {
		hero := data.Hero{}

		if err := c.BodyParser(&hero); err != nil {
			return err
		}

		service.AddHero(hero)

		return c.SendStatus(200)
	})

	log.Fatal(app.Listen(*listerAddr))
}
