package services

import (
	"gitlab.com/VFay/starcraft-wiki/data"
)

type Repository interface {
	Init()
	GetAllHeroes() []data.Hero
	GetHero(id string) data.Hero
	AddHero(hero data.Hero)
}

type Service interface {
	Init()
	GetHeroes() []data.Hero
	GetHero(id string) data.Hero
	AddHero(hero data.Hero)
}

type service struct {
	r Repository
}

func NewService(r Repository) Service {
	return &service{r}
}

func (s *service) Init() {
	s.r.Init()
}

func (s *service) GetHeroes() []data.Hero {
	return s.r.GetAllHeroes()
}

func (s *service) GetHero(id string) data.Hero {
	return s.r.GetHero(id)
}

func (s *service) AddHero(hero data.Hero) {
	s.r.AddHero(hero)
}
